//
//  DetailsViewController.swift
//  Madeinweb Teste
//
//  Created by Bruno Rocca on 22/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController{
    
    
    //MARK: - Outlets
    @IBOutlet weak var backArrow: UIImageView!
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var views: UILabel!
    
    //MARK: - Parameters
    var image: UIImageView!
    var titleVideo: UILabel!
    var videoId: String!
    var descriptionVideo: UILabel!
    var api = APIControl()
    
    var titleVideoInitialPosition: CGPoint!
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTapGestures()
        
        addToViewInitialItems()
        animateInitialItems()
        
        //after animations
        Timer.scheduledTimer(withTimeInterval: 1.2, repeats: false){_ in
            self.loadVideoInfos()
            self.createConstraints()
        }
    }
    
    //MARK: - Methods
    func addTapGestures(){
        let ArrowTap = UITapGestureRecognizer(target: self, action: #selector(backButtonClick(tapGestureRecognizer:)))
        backArrow.isUserInteractionEnabled = true
        backArrow.addGestureRecognizer(ArrowTap)
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(backButtonClick(tapGestureRecognizer:)))
        backLabel.isUserInteractionEnabled = true
        backLabel.addGestureRecognizer(labelTap)
    }
    func createConstraints(){
        //image
        image.translatesAutoresizingMaskIntoConstraints = false
        var imageConstraints = Constraints()
        
        imageConstraints.top = image.topAnchor.constraint(equalTo: (image.superview?.topAnchor)!, constant: 51)
        imageConstraints.right = image.superview?.trailingAnchor.constraint(equalTo: image.trailingAnchor, constant: 0)
        imageConstraints.left = image.leadingAnchor.constraint(equalTo: ((image.superview?.leadingAnchor)!), constant: 0)
        imageConstraints.bottom = image.superview?.bottomAnchor.constraint(equalTo: image.bottomAnchor, constant: 406.1)
        
        imageConstraints.top.isActive = true
        imageConstraints.left.isActive = true
        imageConstraints.right.isActive = true
        imageConstraints.bottom.isActive = true
        
        //titleVideo
        titleVideo.translatesAutoresizingMaskIntoConstraints = false
        var titleVideoConstraints = Constraints()
        
        titleVideoConstraints.top = titleVideo.topAnchor.constraint(equalTo: (image.bottomAnchor), constant: 9.1)
        titleVideoConstraints.right = titleVideo.superview?.trailingAnchor.constraint(equalTo: titleVideo.trailingAnchor, constant: 8.5)
        titleVideoConstraints.left = titleVideo.leadingAnchor.constraint(equalTo: ((titleVideo.superview?.leadingAnchor)!), constant: 8.5)
        
        titleVideoConstraints.top.isActive = true
        titleVideoConstraints.left.isActive = true
        titleVideoConstraints.right.isActive = true
        
        //views
        views.translatesAutoresizingMaskIntoConstraints = false
        var viewsConstraints = Constraints()
        
        viewsConstraints.top = views.topAnchor.constraint(equalTo: (titleVideo.bottomAnchor), constant: 8)
        viewsConstraints.right = views.superview?.trailingAnchor.constraint(equalTo: views.trailingAnchor, constant: 8.5)
        viewsConstraints.left = views.leadingAnchor.constraint(equalTo: ((views.superview?.leadingAnchor)!), constant: 8.5)
        
        viewsConstraints.top.isActive = true
        viewsConstraints.left.isActive = true
        viewsConstraints.right.isActive = true
        
        
        //description
        descriptionVideo.translatesAutoresizingMaskIntoConstraints = false
        var descriptionVideoConstrainsts = Constraints()
        
        descriptionVideo.setContentHuggingPriority(UILayoutPriority.defaultHigh, for:.horizontal)
        descriptionVideo.setContentHuggingPriority(UILayoutPriority(1000), for:.vertical)
        descriptionVideo.setContentCompressionResistancePriority(UILayoutPriority.defaultHigh, for: .horizontal)
        descriptionVideo.setContentCompressionResistancePriority(UILayoutPriority.defaultHigh, for: .vertical)
        
        
        descriptionVideoConstrainsts.bottom = descriptionVideo.bottomAnchor.constraint(equalTo: (descriptionVideo.superview?.bottomAnchor)!, constant: 373)
        descriptionVideoConstrainsts.bottom.priority = UILayoutPriority(rawValue: 250)
        descriptionVideoConstrainsts.top = descriptionVideo.topAnchor.constraint(equalTo: (views.bottomAnchor), constant: 7)
        descriptionVideoConstrainsts.right = descriptionVideo.superview?.trailingAnchor.constraint(equalTo: descriptionVideo.trailingAnchor, constant: 8.5)
        descriptionVideoConstrainsts.left = descriptionVideo.leadingAnchor.constraint(equalTo: ((descriptionVideo.superview?.leadingAnchor)!), constant: 8.5)

        descriptionVideoConstrainsts.top.isActive = true
        descriptionVideoConstrainsts.bottom.isActive = true
        descriptionVideoConstrainsts.left.isActive = true
        descriptionVideoConstrainsts.right.isActive = true
    }
    
    func loadVideoInfos(){
        api.getDetailsRequest(videoId: videoId){rawVideoDetails in
            guard let videoDetails = rawVideoDetails else{
                NSLog("ERROR: Details request returned a nil video details")
                return
            }
            
            self.titleVideo.text = videoDetails.items[0].snippet.title
            self.views.text = videoDetails.items[0].statistics.viewCount + " visualizações"
            self.descriptionVideo.text = videoDetails.items[0].snippet.description
        }
    }
    func addToViewInitialItems(){
        self.view.addSubview(titleVideo)
        self.view.addSubview(descriptionVideo)
        self.view.addSubview(image)
        
        views.frame.origin.y += 50
    }
    func animateInitialItems(){
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false){_ in
            UIView.animate(withDuration: 1){
                self.titleVideo.frame = CGRect(x: 9, y: 270, width: 357, height: 24)
                self.titleVideo.font = UIFont.boldSystemFont(ofSize: 20)
                
                self.descriptionVideo.frame = CGRect(x: 9, y: 326, width: 357, height: 16)
                
                self.image.frame = CGRect(x: 0, y: 50, width: 375, height: 210)
                
                self.views.frame.origin.y -= 50
            }
        }
    }
    
    //MARK: - Actions
    @objc func backButtonClick(tapGestureRecognizer: UITapGestureRecognizer){
        self.dismiss(animated: false, completion: nil);
    }
}
