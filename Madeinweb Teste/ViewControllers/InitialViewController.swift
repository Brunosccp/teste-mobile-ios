//
//  ViewController.swift
//  Madeinweb Teste
//
//  Created by Bruno Rocca on 21/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Outlets
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    //MARK: - Parameters
    let api = APIControl()
    var search: Search?
    let activityIndicator = UIActivityIndicatorView(style: .white)
    
    var tableMode = false
    var tableView = UITableView()
    
    var logoConstraints = Constraints()
    var searchFieldConstraints = Constraints()
    var searchButtonConstraints = Constraints()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGestures()
        createConstraints()
        createActivityIndicator()
    }
    
    //MARK: - Methods
    func addTapGestures(){
        let logoTap = UITapGestureRecognizer(target: self, action: #selector(logoAction(tapGestureRecognizer:)))
        logo.isUserInteractionEnabled = true
        logo.addGestureRecognizer(logoTap)
    }
    func createConstraints(){
        //logo constraints
        logo.translatesAutoresizingMaskIntoConstraints = false
        
        logoConstraints.bottom = logo.superview?.bottomAnchor.constraint(equalTo: logo.bottomAnchor, constant:430)
        logoConstraints.bottom.priority = UILayoutPriority(rawValue: 750)
        logoConstraints.top = logo.topAnchor.constraint(equalTo: ((logo.superview?.topAnchor)!), constant: 175)
        logoConstraints.right = logo.superview?.trailingAnchor.constraint(equalTo: logo.trailingAnchor, constant: 62)
        logoConstraints.left = logo.leadingAnchor.constraint(equalTo: ((logo.superview?.leadingAnchor)!), constant: 48)
            
        logoConstraints.width = NSLayoutConstraint(item: logo, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 265)
        logoConstraints.height = NSLayoutConstraint(item: logo, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 62)
            
        logo.addConstraints([logoConstraints.width, logoConstraints.height])
            
        logoConstraints.top.isActive = true
        logoConstraints.bottom.isActive = true
        logoConstraints.left.isActive = true
        logoConstraints.right.isActive = true
        
        //search field constraints
        searchField.translatesAutoresizingMaskIntoConstraints = false

        searchFieldConstraints.bottom = logo.superview?.bottomAnchor.constraint(equalTo: searchField.bottomAnchor, constant:294)
        searchFieldConstraints.bottom.priority = UILayoutPriority(rawValue: 750)
        searchFieldConstraints.top = searchField.topAnchor.constraint(equalTo: ((searchField.superview?.topAnchor)!), constant: 321)
        searchFieldConstraints.right = logo.superview?.trailingAnchor.constraint(equalTo: searchField.trailingAnchor, constant: 12.3)
        searchFieldConstraints.left = searchField.leadingAnchor.constraint(equalTo: ((searchField.superview?.leadingAnchor)!), constant: 14.9)

        searchFieldConstraints.width = NSLayoutConstraint(item: searchField, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 347.8)
        searchFieldConstraints.height = NSLayoutConstraint(item: searchField, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 52)

        searchField.addConstraints([searchFieldConstraints.width, searchFieldConstraints.height])

        searchFieldConstraints.top.isActive = true
        searchFieldConstraints.bottom.isActive = true
        searchFieldConstraints.left.isActive = true
        searchFieldConstraints.right.isActive = true
        
        //search button constraints
        searchButton.translatesAutoresizingMaskIntoConstraints = false

        searchButtonConstraints.bottom = logo.superview?.bottomAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 229)
        searchButtonConstraints.bottom.priority = UILayoutPriority(rawValue: 750)
        searchButtonConstraints.top = searchButton.topAnchor.constraint(equalTo: ((searchButton.superview?.topAnchor)!), constant: 386)
        searchButtonConstraints.right = logo.superview?.trailingAnchor.constraint(equalTo: searchButton.trailingAnchor, constant: 12)
        searchButtonConstraints.left = searchButton.leadingAnchor.constraint(equalTo: ((searchButton.superview?.leadingAnchor)!), constant: 15)

        searchButtonConstraints.width = NSLayoutConstraint(item: searchButton, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 348)
        searchButtonConstraints.height = NSLayoutConstraint(item: searchButton, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 52)

        searchButton.addConstraints([searchButtonConstraints.width, searchButtonConstraints.height])

        searchButtonConstraints.top.isActive = true
        searchButtonConstraints.bottom.isActive = true
        searchButtonConstraints.left.isActive = true
        searchButtonConstraints.right.isActive = true
        
    }
    func createActivityIndicator(){
        activityIndicator.center = CGPoint(x: 177, y: 383)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = UIColor(red: 33/255, green: 123/255, blue: 224/255, alpha: 1)
        view.addSubview(activityIndicator)
    }
    //function to change the view after click the search button
    func changeView(){
        animateView()
    }
    func animateView(){
        if(tableMode == false){
            UIView.animate(withDuration: 1.0) {
                self.logo.frame = CGRect(x: 128, y: 15, width: 115, height: 27)
                self.searchField.frame = CGRect(x: 11, y: 60, width: 320.1, height: 41.9)
                
                self.searchButton.frame = CGRect(x: 287.7, y: 60, width: 78.3, height: 41.9)
                self.searchButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
                
                //logo constraints
                self.logoConstraints.top.constant = 15
                self.logoConstraints.left.constant = 128
                self.logoConstraints.bottom.constant = 625
                self.logoConstraints.right.constant = 132
                self.logoConstraints.height.constant = 27
                self.logoConstraints.width.constant = 115
                
                //searchField constraints
                self.searchFieldConstraints.top.constant = 60
                self.searchFieldConstraints.left.constant = 11
                self.searchFieldConstraints.right.constant = 43.9
                self.searchFieldConstraints.bottom.constant = 565.1
                self.searchFieldConstraints.height.constant = 41.9
                self.searchFieldConstraints.width.constant = 320.1
                
                //searchButton constraints
                self.searchButtonConstraints.top.constant = 60
                self.searchButtonConstraints.left.constant = 287.7
                self.searchButtonConstraints.right.constant = 9
                self.searchButtonConstraints.bottom.constant = 565.1
                self.searchButtonConstraints.height.constant = 41.9
                self.searchButtonConstraints.width.constant = 78.3
            }
        }else{
            UIView.animate(withDuration: 1.0) {
                self.logo.frame = CGRect(x: 55, y: 175, width: 265, height: 62)
                self.searchField.frame = CGRect(x: 13, y: 321, width: 348, height: 52)
                
                self.searchButton.frame = CGRect(x: 13, y: 385, width: 348, height: 52)
                self.searchButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
                
                //logo constraints
                self.logoConstraints.top.constant = 155
                self.logoConstraints.left.constant = 48
                self.logoConstraints.bottom.constant = 430
                self.logoConstraints.right.constant = 62
                self.logoConstraints.height.constant = 62
                self.logoConstraints.width.constant = 265
                
                //searchBar constraints
                self.searchFieldConstraints.top.constant = 321
                self.searchFieldConstraints.left.constant = 14.9
                self.searchFieldConstraints.bottom.constant = 321
                self.searchFieldConstraints.right.constant = 12.3
                self.searchFieldConstraints.height.constant = 52
                self.searchFieldConstraints.width.constant = 347.8
                
                //searchButton constraints
                self.searchButtonConstraints.top.constant = 385.9
                self.searchButtonConstraints.left.constant = 15
                self.searchButtonConstraints.bottom.constant = 229.1
                self.searchButtonConstraints.right.constant = 12
                self.searchButtonConstraints.height.constant = 52
                self.searchButtonConstraints.width.constant = 348
            }
        }
    }
    
    //MARK: - TableView Protocol Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(search == nil){
            NSLog("TableView can't initializate because there's no search data")
            return 0
        }
        return search!.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        if(search == nil){
            NSLog("TableView can't initializate because there's no search data")
            return cell
        }
        
        cell.title.text = search!.items[indexPath.row].snippet.title
        cell.descriptionVideo.text = search!.items[indexPath.row].snippet.description
        let url = search!.items[indexPath.row].snippet.thumbnails.high.url
        cell.imageVideo!.imageFromURL(urlString: url)
        
        return cell
    }
    
    //MARK: - TableView Methods
    func createTableView(){
        let barHeight: CGFloat = 120
        let width = self.view.frame.width
        let height = self.view.frame.height - barHeight
        
        tableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: width, height: height))
        
        let videoCell = UINib.init(nibName: "VideoCell", bundle: nil)
        tableView.register(videoCell.self, forCellReuseIdentifier: "VideoCell")
        
        tableView.separatorStyle = .none
        
        
        self.view.addSubview(tableView)

        //creating table view constraints
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        var tableViewConstraints = Constraints()
        
        tableViewConstraints.bottom = tableView.superview?.bottomAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0)
        tableViewConstraints.top = tableView.topAnchor.constraint(equalTo: (tableView.superview?.topAnchor)!, constant: 120)
        tableViewConstraints.right = tableView.superview?.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: 0)
        tableViewConstraints.left = tableView.leadingAnchor.constraint(equalTo: ((tableView.superview?.leadingAnchor)!), constant: 0)
        
        tableViewConstraints.top.isActive = true
        tableViewConstraints.bottom.isActive = true
        tableViewConstraints.left.isActive = true
        tableViewConstraints.right.isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(108)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellTapped = indexPath.row
        
        performSegue(withIdentifier: "ToDetails", sender: cellTapped)
    }
    
    //MARK: - Prepare For Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cellTapped = sender as! Int
        
        let nextViewController = segue.destination as! DetailsViewController
        nextViewController.videoId = search!.items[cellTapped].id.videoId
        
        let cell = tableView.cellForRow(at: IndexPath(row: cellTapped, section: 0)) as! VideoCell
        
        //getting image
        guard let imagePosition = cell.title.superview?.convert(cell.imageVideo.frame.origin, to: nil) else{
            NSLog("ERROR: Cannot get Image Position before segue")
            return
        }
        let nextImageVideo = UIImageView()
        nextImageVideo.frame = CGRect(x: imagePosition.x, y: imagePosition.y, width: cell.imageVideo.frame.width, height: cell.imageVideo.frame.height)
        nextImageVideo.image = cell.imageVideo.image
        
        nextViewController.image = nextImageVideo
        
        //getting title
        guard let titlePosition = cell.title.superview?.convert(cell.title.frame.origin, to: nil) else{
            NSLog("ERROR: Cannot get Title Position before segue")
            return
        }
        let nextTitleVideo = UILabel(frame: CGRect(x: titlePosition.x, y: titlePosition.y, width: cell.title.frame.width, height: cell.title.frame.height))
        nextTitleVideo.textColor = cell.title.textColor
        nextTitleVideo.font = cell.title.font
        nextTitleVideo.text = cell.title.text
        
        nextViewController.titleVideo = nextTitleVideo
        
        //getting description
        guard let descriptionPosition = cell.descriptionVideo.superview?.convert(cell.descriptionVideo.frame.origin, to: nil) else{
            NSLog("ERROR: Cannot get Description Position before segue")
            return
        }
        
        let nextDescriptionVideo = UILabel(frame: CGRect(x: descriptionPosition.x, y: descriptionPosition.y, width: cell.descriptionVideo.frame.width, height: cell.descriptionVideo.frame.height))
        nextDescriptionVideo.textColor = cell.descriptionVideo.textColor
        nextDescriptionVideo.font = cell.descriptionVideo.font
        nextDescriptionVideo.text = cell.descriptionVideo.text
        
        nextViewController.descriptionVideo = nextDescriptionVideo
    }
    
    //MARK: - Actions
    @IBAction func buttonAction() {
        if let query = searchField.text{
            
            if(!tableMode){
                self.changeView()
            }else{
                tableView.removeFromSuperview()
            }
            
            self.tableMode = true
            
            //wait for animation to do request
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false){_ in
                self.activityIndicator.startAnimating()
                
                self.api.getSearchRequest(query: query){search in
                    self.search = search
                    self.createTableView()
                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    @objc func logoAction(tapGestureRecognizer: UITapGestureRecognizer){
        if(tableMode){
            tableView.removeFromSuperview()
            
            animateView()
            
            tableMode = false
            
        }
    }
}



