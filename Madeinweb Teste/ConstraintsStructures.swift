//
//  ConstraintsStructures.swift
//  Madeinweb Teste
//
//  Created by Bruno Rocca on 22/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation
import UIKit

struct Constraints{
    var bottom: NSLayoutConstraint!
    var top: NSLayoutConstraint!
    var right: NSLayoutConstraint!
    var left: NSLayoutConstraint!
    var width: NSLayoutConstraint!
    var height: NSLayoutConstraint!
}
struct SearchFieldConstraints{
    var bottom: NSLayoutConstraint?
    var top: NSLayoutConstraint?
    var right: NSLayoutConstraint?
    var left: NSLayoutConstraint?
    var width: NSLayoutConstraint?
    var height: NSLayoutConstraint?
}

