//
//  VideoCell.swift
//  Madeinweb Teste
//
//  Created by Bruno Rocca on 22/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation
import UIKit

class VideoCell: UITableViewCell {
    @IBOutlet weak var imageVideo: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionVideo: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
