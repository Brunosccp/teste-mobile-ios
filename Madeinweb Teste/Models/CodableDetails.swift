//
//  CodableDetails.swift
//  Madeinweb Teste
//
//  Created by Bruno Rocca on 21/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation

struct Details: Decodable{
    let items: Array<ItemsDetails>
    
}
struct ItemsDetails: Decodable{
    let snippet: SnippetDetails
    let statistics: StatisticsDetails
}
struct SnippetDetails: Decodable{
    let title: String
    let description: String
}
struct StatisticsDetails: Decodable{
    let viewCount: String
}
