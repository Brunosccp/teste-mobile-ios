//
//  CodableStructures.swift
//  madeinwebTest
//
//  Created by Bruno Rocca on 21/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation

struct Search: Decodable{
    let items: Array<ItemsSearch>
}
struct ItemsSearch: Decodable{
    let snippet: SnippetSearch
    let id: IdSearch
}
struct IdSearch: Decodable{
    let videoId: String
}
struct SnippetSearch: Decodable{
    let title: String
    let description: String
    let thumbnails: ThumbnailsSearch
}
struct ThumbnailsSearch: Decodable{
    let high: High
}
struct High: Decodable{
    let url: String
}



