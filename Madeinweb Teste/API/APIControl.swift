//
//  APIControl.swift
//  madeinwebTest
//
//  Created by Bruno Rocca on 21/03/19.
//  Copyright © 2019 Bruno Rocca. All rights reserved.
//

import Foundation
import Alamofire

class APIControl{
    
    //MARK: - Parameters
    let key = "AIzaSyBsFZYoFcSeISqpgI_YsmjcQekOpzvW98Q"
    let key2 = "AIzaSyDOeJ_8RJ42KmaJXYEU92deMctoiHQe7OU"
    let key3 = "AIzaSyAxT1Ct2D0jiesBnmxPfNTEZ30smZX5tII"


    
    //MARK: - Search Methods
    func getSearchRequest(query: String, completion: @escaping(_ search: Search?) -> Void){
        let urlQuery = query.replacingOccurrences(of: " ", with: "+")
        
        
        let url = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=\(urlQuery)&maxResults=10&type=video&key=\(key3)"
        
        Alamofire.request(url).responseJSON{ response in
            guard let status = response.response?.statusCode else{
                NSLog("ERROR: Could not get response")
                completion(nil)
                return
            }
            
            switch(status){
            //put cases from status code return
            case 200:
                print("SUCCESS")
                
                let search = self.getSearchCodable(data: response.data)
                completion(search)
            case 400:
                NSLog("ERROR: Key status error")
                completion(nil)
                return
            default:
                NSLog("ERROR: Unknown status error")
                completion(nil)
                return
            }
        }
    }
    private func getSearchCodable(data: Data?) -> Search?{
        do{
            let search = try JSONDecoder().decode(Search.self, from: data!)
            return search
        }catch{
            NSLog("ERROR: Decoding to Example - \(error)")
            return nil
        }
    }
    
    //MARK: - Video Details Methods
    func getDetailsRequest(videoId: String, completion: @escaping(_ details: Details?) -> Void){
        let url = "https://www.googleapis.com/youtube/v3/videos?id=\(videoId)&part=snippet,statistics&key=\(key3)"
        
        Alamofire.request(url).responseJSON{ response in
            guard let status = response.response?.statusCode else{
                NSLog("ERROR: Could not get response")
                completion(nil)
                return
            }
            
            switch(status){
            //put cases from status code return
            case 200:
                print("SUCCESS")
                
                let details = self.getDetailsCodable(data: response.data)
                completion(details)
            case 400:
                NSLog("ERROR: Key status error")
                completion(nil)
                return
            default:
                NSLog("ERROR: Unknown status error")
                completion(nil)
                return
            }
        }
    }
    private func getDetailsCodable(data: Data?) -> Details?{
        do{
            let search = try JSONDecoder().decode(Details.self, from: data!)
            return search
        }catch{
            NSLog("ERROR: Decoding to Example - \(error)")
            return nil
        }
    }
    
}
